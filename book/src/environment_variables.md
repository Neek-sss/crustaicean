# Set Environment Variables:

* **BAR_DIR** = BAR Install Directory
* **BAR_ENGINE** = Engine version being used (can be found in the BAR
  install directory under `data/engine`; choose the largest version number; **do not** include the " bar" portion)

Example:

* **BAR_DIR** = C:\Program Files\Beyond-All-Reason
* **BAR_ENGINE** = 105.1.1-2414-g91273e4