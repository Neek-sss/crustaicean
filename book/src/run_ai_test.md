# Running

## Testing the AI

### Set Envrionment Variables

See [Envrionment Variables](environment_variables.md)

### Update Gametype:

Open BAR and run any standard skirmish match (you do not need to finish the game, just start and exit).

Navigate to the `data` folder within the BAR install directory,
open the `_script.txt` file, and find the line that is similar to this:

TODO: May be able to get from the UI via string under map in game selection

```
gametype=Beyond All Reason test-25565-0373916;
```

Edit the matching line in the `crustaicean` _script.txt to match.

Edit

```shell
cargo make test-ai
```
