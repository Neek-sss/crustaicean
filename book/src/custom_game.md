# Running a Custom Game

Due to a recent change in Beyond All Reason, the game does not
recognize custom AI's from the lobby. Because of this there are
some extra steps to run a custom game using the CRustAIcean AI,
namely replacing the NullAI with CRustAIcean. The actual
replacement is performed by `cargo make` (explained below).
The only steps you need to performe to run a game is:

1. Install cargo make via `cargo install cargo-make`
2. Set the `BAR_DIR` and `BAR_ENGINE` environment variables
   (See [Envrionment Variables](environment_variables.md))
3. Run `cargo make install-fake-null-ai`
4. Start a new game with an InactiveAI (this is actually CRustAIcean)

## Steps Performed by Cargo Make

### Step 1: Backup NullAI

* Navigate to `<Beyond All Reason Install Directory>/data/engine<Engine Directory>/AI/Skirmish`
* Create a backup folder
* Move NullAI into the backup folder

### Step 2: Build CRustAIcean

* Build CRustAIcean via `cargo build`
* Collect CRustAIcean library, NullAI_AIInfo.lua, and AIOptions.lua
  into AI package

### Step 2: Install CRustAIcean

* Copy the package into the Skirmish folder and rename
