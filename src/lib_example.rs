#![allow(non_snake_case)]

use lazy_static::lazy_static;
use slog::{info, Logger};
use spring_ai_rs::ai_interface::callback::command::options::UnitCommandOptions;
use spring_ai_rs::ai_interface::callback::facing::Facing;
use spring_ai_rs::ai_interface::callback::position::GroundPosition;
use spring_ai_rs::ai_interface::callback::teams::Team;
use spring_ai_rs::ai_interface::callback::unit::Unit;
use spring_ai_rs::ai_interface::callback::unit_def::easy_name::EasyUnitName;
use spring_ai_rs::ai_interface::callback::weapon_def::WeaponDef;
use spring_ai_rs::{
    ai_interface::{
        callback::{command::command_topic::CommandTopic, group::Group},
        AIInterface,
    },
    ai_macro::ai_func,
};

#[ai_func("init")]
fn init(_logger: &Logger, _ai_interface: AIInterface) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("release")]
fn release(_logger: &Logger, _ai_interface: AIInterface) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("message")]
fn message(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _player: i32,
    _message: &str,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("command_finished")]
fn command_finished(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _command_id: i32,
    _command_topic: CommandTopic,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_created")]
fn enemy_created(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_damaged")]
fn enemy_damaged(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
    _attacker: Unit,
    _direction: [f32; 3],
    _weapon_def: WeaponDef,
    _paralyzer: bool,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_destroyed")]
fn enemy_destroyed(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
    _attacker: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_enter_los")]
fn enemy_enter_los(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_enter_radar")]
fn enemy_enter_radar(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_finished")]
fn enemy_finished(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_leave_los")]
fn enemy_leave_los(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("enemy_leave_radar")]
fn enemy_leave_radar(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _enemy: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("load")]
fn load(_logger: &Logger, _ai_interface: AIInterface, _file: &str) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("lua_message")]
fn lua_message(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _message: &str,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("null")]
fn null(_logger: &Logger, _ai_interface: AIInterface) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("player_command")]
fn player_command(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit_ids: &[i32],
    _command_topic: CommandTopic,
    _player_id: i32,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("save")]
fn save(_logger: &Logger, _ai_interface: AIInterface, _file: &str) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("seismic_ping")]
fn seismic_ping(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _position: [f32; 3],
    _strength: f32,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_captured")]
fn unit_captured(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _old_team: Team,
    _new_team: Team,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_created")]
fn unit_created(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _builder: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_damaged")]
fn unit_damaged(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _attacker: Unit,
    _damage: f32,
    _direction: [f32; 3],
    _weapon_def: WeaponDef,
    _paralyzer: bool,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_destroyed")]
fn unit_destroyed(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _attacker: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_finished")]
fn unit_finished(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_given")]
fn unit_given(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _old_team: Team,
    _new_team: Team,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_idle")]
fn unit_idle(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("unit_move_failed")]
fn unit_move_failed(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("update")]
fn update(_logger: &Logger, _ai_interface: AIInterface, _frame: i32) -> Result<(), Box<dyn Error>> {
    Ok(())
}

#[ai_func("weapon_fired")]
fn weapon_fired(
    _logger: &Logger,
    _ai_interface: AIInterface,
    _unit: Unit,
    _weapon_def: WeaponDef,
) -> Result<(), Box<dyn Error>> {
    Ok(())
}
