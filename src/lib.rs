#![allow(non_snake_case)]

pub mod init;
pub mod project_selection;
pub mod shared;
pub mod unit_created;
pub mod unit_destroyed;
pub mod unit_finished;
pub mod update;

use rand::Rng;
use serde::{Deserialize, Serialize};
use spring_ai_rs::ai_interface::callback::unit_def::kind::{arm::Arm, core::Core};

spring_ai_rs::activate_ai!();

#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum AIType {
    SimpleAI,
    SimpleCheaterAI,
    SimpleDefenderAI,
    SimpleConstructorAI,
}

pub const BAD_ARM_UNITS: [Arm; 7] = [
    // Transports
    Arm::T1AirTransport,
    Arm::HeavyTransport,
    // Stockpilers
    Arm::NuclearSilo,
    Arm::EMPMissileLauncher,
    Arm::AntiNukeLauncher,
    // Minelayer
    Arm::Minelayer,
    // Depth Charge
    // Walls
    Arm::FortificationWall,
];
pub const BAD_CORE_UNITS: [Core; 7] = [
    // Transports
    Core::T1AirTransport,
    Core::HeavyTransport,
    // Stockpilers
    Core::NuclearSilo,
    Core::TacticalMissileLauncher,
    Core::AntiNukeLauncher,
    // Minelayer
    Core::Minelayer,
    // Depth Charge
    // Walls
    Core::FortificationWall,
];

pub fn random_choice<T: Clone>(vec: &[T]) -> T {
    let mut rng = rand::thread_rng();
    let index = rng.gen_range(0..vec.len());
    vec[index].clone()
}

pub fn random_number(start: f32, end: f32) -> f32 {
    let range = end - start;
    let mut rng = rand::thread_rng();
    let rng_val = rng.gen::<f32>();
    (rng_val * range) + start
}
