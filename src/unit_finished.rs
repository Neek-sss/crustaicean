use std::error::Error;

use slog::{info, Logger};
use spring_ai_rs::{
    ai_interface::{
        callback::{fire_state::FireState, move_state::MoveState, unit::Unit},
        AIInterface,
    },
    ai_macro::ai_func,
};

#[ai_func("unit_finished")]
fn unit_finished(
    logger: &Logger,
    ai_interface: AIInterface,
    unit: Unit,
    builder: Unit,
) -> Result<(), Box<dyn Error>> {
    info!(logger, "Finished a unit");

    unit.set_fire_state(&[], None, None, FireState::FireAtWill)?;
    unit.set_move_state(&[], None, None, MoveState::Roam)?;

    Ok(())
}
