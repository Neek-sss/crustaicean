use std::error::Error;

use slog::{info, Drain, Level, Logger};
use spring_ai_rs::{
    ai_interface::{callback::unit_def::kind::any::Any, AIInterface},
    ai_macro::ai_func,
};

use crate::{random_choice, AIType};

#[ai_func("init")]
fn init(logger: &Logger, ai_interface: AIInterface) -> Result<(), Box<dyn Error>> {
    // TODO: Make random or setting
    let ai_type = random_choice(&[
        AIType::SimpleAI,
        // AIType::SimpleConstructorAI,
        // AIType::SimpleDefenderAI,
        // AIType::SimpleCheaterAI,
    ]);
    ai_interface.store_memory("AIType", &ai_type);

    // TODO: CheaterAI Crashes
    // if ai_type == AIType::SimpleCheaterAI {
    //     ai_interface.cheat().enable()?;
    // }

    let defs = ai_interface.unit_interface().get_unit_definitions()?;
    let resources = ai_interface.resource_interface().get_resources()?;

    let mut commander_defs = Vec::new();
    let mut factory_defs = Vec::new();
    let mut constructor_defs = Vec::new();
    let mut extractor_defs = Vec::new();
    let mut generator_defs = Vec::new();
    // let mut converter_defs = Vec::new(); // TODO: Converters (requires custom parameters)
    let mut turret_defs = Vec::new();
    let mut undefined_building_defs = Vec::new();
    let mut undefined_unit_defs = Vec::new();

    for &unit_def in &defs {
        if vec![
            Any::T1AirTransport,
            Any::HeavyTransport,
            Any::AircraftCarrier,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
        } else if unit_def == Any::Commander {
            commander_defs.push(unit_def);
        } else if vec![
            Any::AirPlant,
            Any::BotLab,
            Any::VehiclePlant,
            Any::HovercraftPlatform,
            Any::Shipyard,
            Any::AdvancedAircraftPlant,
            Any::AdvancedBotLab,
            Any::AdvancedVehiclePlant,
            Any::AdvancedShipyard,
            Any::ExperimentalGantry,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
            factory_defs.push(unit_def);
        } else if vec![
            Any::T1AirConstructor,
            Any::T1BotConstructor,
            Any::T1VehicleConstructor,
            Any::AmphibiousConstructor,
            Any::SeaplaneConstructor,
            Any::T1HovercraftConstructor,
            Any::T1ShipConstructor,
            Any::T2AirConstructor,
            Any::T2BotConstructor,
            Any::T2VehicleConstructor,
            Any::T2ShipConstructor,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
            constructor_defs.push(unit_def);
        } else if vec![
            Any::MetalExtractor,
            Any::AdvancedMetalExtractor,
            Any::ArmedMetalExtractor,
            Any::AdvancedArmoredMetalExtractor,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
            extractor_defs.push(unit_def);
        } else if vec![
            Any::Solar,
            Any::AdvancedSolar,
            Any::WindTurbine,
            Any::TidalGenerator,
            Any::AdvancedFusionReactor,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
            generator_defs.push(unit_def);
        } else if vec![
            Any::LaserTower,
            Any::AreaControlLaserTower,
            Any::LightLaserTower,
            Any::LightAntiAir,
            Any::HeavyAntiAir,
            Any::LongRangeAntiAir,
            Any::Artillery,
            Any::AntiAirFlak,
            Any::ElementalTurret,
            Any::PopUpPlasmaArtillery,
            Any::LongRangePlasmaCannon,
            Any::RapidFireLongRangePlasmaCannon,
            Any::FloatingLightAntiAir,
            Any::FloatingHeavyLaserTower,
            Any::FloatingMultiweaponPlatform,
        ]
        .into_iter()
        .any(|a| a == unit_def)
        {
            turret_defs.push(unit_def);
        } else if !unit_def.movement().able_to_move()? {
            undefined_building_defs.push(unit_def);
        } else {
            undefined_unit_defs.push(unit_def);
        }
    }

    ai_interface.store_memory("SimpleCommanderDefs", &commander_defs);
    ai_interface.store_memory("SimpleFactoryDefs", &factory_defs);
    ai_interface.store_memory("SimpleConstructorDefs", &constructor_defs);
    ai_interface.store_memory("SimpleExtractorDefs", &extractor_defs);
    ai_interface.store_memory("SimpleGeneratorDefs", &generator_defs);
    ai_interface.store_memory("SimpleTurretDefs", &turret_defs);
    ai_interface.store_memory("SimpleUndefinedBuildingDefs", &undefined_building_defs);
    ai_interface.store_memory("SimpleUndefinedUnitDefs", &undefined_unit_defs);

    info!(logger, "Initialized CRustAIcean");

    Ok(())
}
