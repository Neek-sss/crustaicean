use std::error::Error;

use spring_ai_rs::ai_interface::{
    callback::{
        command::options::UnitCommandOptions,
        facing::Facing,
        position::GroundPosition,
        unit::Unit,
        unit_def::{kind::any::Any, UnitDef},
    },
    AIInterface,
};

pub fn solar_array(
    unit: Unit,
    position: [f32; 3],
    height: i32,
    width: i32,
) -> Result<(), Box<dyn Error>> {
    let mut building_def_opt = None;
    let ai_interface = AIInterface { ai_id: unit.ai_id };
    for def in ai_interface.unit_interface().get_unit_definitions()? {
        if def.kind()? == Any::Solar {
            building_def_opt = Some(def);
        }
    }
    if let Some(building_def) = building_def_opt {
        let building_width = building_def.miscellaneous().x_size()? as f32;
        let building_height = building_def.miscellaneous().z_size()? as f32;
        for i in 0..height {
            for j in 0..width {
                unit.build(
                    &[UnitCommandOptions::ShiftKey],
                    None,
                    None,
                    Any::Solar,
                    GroundPosition(
                        position[0] + (building_width * j as f32),
                        position[2] - (building_height * i as f32),
                    ),
                    Facing::None,
                )?;
            }
        }
    }

    Ok(())
}

pub fn sorted_distance_to(
    locations: &[(f32, f32)],
    position: [f32; 3],
) -> Result<Vec<((f32, f32), f32)>, Box<dyn Error>> {
    let mut ret = Vec::new();
    for &location in locations {
        ret.push((
            location,
            ((location.0 - position[0]).powi(2) + (location.1 - position[2]).powi(2)).sqrt(),
        ))
    }
    ret.sort_by(|(_, first), (_, second)| first.partial_cmp(second).unwrap());

    Ok(ret)
}

pub fn area_mex<DK>(unit: Unit, extractor_unit_name: DK, radius: f32) -> Result<(), Box<dyn Error>>
where
    DK: PartialEq<UnitDef> + Copy,
    UnitDef: PartialEq<DK>,
{
    let ai_interface = AIInterface { ai_id: unit.ai_id };

    for def in unit.unit_def()?.building().build_options()? {
        if def == extractor_unit_name {
            // Build mex's
            for resource in ai_interface.resource_interface().get_resources()? {
                if def.resource(resource).extracts()? > 0.0 {
                    let position = unit.position()?;
                    let resource_locations = ai_interface
                        .map()
                        .resource_spots_map(resource)?
                        .into_iter()
                        .map(|(location, _)| location)
                        .collect::<Vec<_>>();
                    let mut resource_locations_in_radius =
                        sorted_distance_to(&resource_locations, position)?
                            .into_iter()
                            .filter_map(|(location, distance)| {
                                if distance < radius {
                                    Some(location)
                                } else {
                                    None
                                }
                            })
                            .rev()
                            .collect::<Vec<_>>();

                    while let Some(start_location) = resource_locations_in_radius.pop() {
                        unit.build(
                            &[UnitCommandOptions::ShiftKey],
                            None,
                            None,
                            extractor_unit_name,
                            GroundPosition(start_location.0, start_location.1),
                            Facing::None,
                        )?;

                        resource_locations_in_radius = sorted_distance_to(
                            &resource_locations_in_radius,
                            [start_location.0, 0.0, start_location.1],
                        )?
                        .into_iter()
                        .map(|(location, _)| location)
                        .rev()
                        .collect();
                    }
                }
            }
        }
    }

    Ok(())
}
