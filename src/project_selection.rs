use std::error::Error;

use slog::{info, Logger};
use spring_ai_rs::ai_interface::{
    callback::{
        command::options::UnitCommandOptions,
        facing::Facing,
        position::GroundPosition,
        unit::Unit,
        unit_def::{kind::any::Any, UnitDef},
    },
    AIInterface,
};

use crate::{random_choice, random_number, shared::sorted_distance_to, AIType};

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ProjectType {
    Commander,
    Constructor,
    Factory,
}

pub fn nearby_units(
    location: (f32, f32),
    radius: f32,
    team_units: &[Unit],
    friendly_units: &[Unit],
    enemy_units: &[Unit],
) -> bool {
    team_units
        .iter()
        .chain(friendly_units.iter())
        .chain(enemy_units.iter())
        .any(|unit| {
            let u_pos = unit.position().unwrap();
            (u_pos[0] - location.0).abs() < radius && (u_pos[2] - location.1).abs() < radius
        })
}

pub fn simple_build_order(
    logger: &Logger,
    ai_interface: AIInterface,
    frame: i32,
    team_units: &[Unit],
    friendly_units: &[Unit],
    enemy_units: &[Unit],
    unit: Unit,
    project: UnitDef,
) -> Result<(), Box<dyn Error>> {
    info!(logger, "Starting simple build order");

    let mut num_tests = 0;
    let position = unit.position()?;

    for _ in 0..10 {
        // for build_near in team_units {
        num_tests += 1;

        let build_near = random_choice(&team_units);
        let ref_def = build_near.unit_def()?;

        let resources = ai_interface.resource_interface().get_resources()?;

        let is_building = !ref_def.movement().able_to_move()?;
        let is_commander = ref_def == Any::Commander;
        let is_extractor = resources
            .into_iter()
            .any(|resource| ref_def.resource(resource).extracts().unwrap() > 0.0);

        if (is_building || is_commander) && !is_extractor {
            let ref_position = build_near.position()?;

            let ref_foot_x = ref_def.miscellaneous().x_size()? as f32;
            let ref_foot_z = ref_def.miscellaneous().z_size()? as f32;

            let spacing = 8.0 * random_number(1.0, 8.0).ceil();

            let direction =
                random_choice(&[Facing::North, Facing::East, Facing::South, Facing::West]);

            let mut rx = 0.0;
            let mut rz = 0.0;

            match direction {
                Facing::North => rz = spacing + ref_foot_z,
                Facing::None | Facing::South => rz = -spacing,
                Facing::East => rx = spacing + ref_foot_x,
                Facing::West => rx = -spacing,
            };

            let nearby_units = nearby_units(
                (ref_position[0] + rx, ref_position[2] + rz),
                ref_foot_z + ref_foot_x,
                team_units,
                friendly_units,
                enemy_units,
            );

            if !nearby_units {
                info!(logger, "Sending build command");
                unit.build(
                    &[UnitCommandOptions::ShiftKey],
                    None,
                    Some(frame + 10000),
                    project,
                    GroundPosition(ref_position[0] + rx, ref_position[2] + rz),
                    Facing::None,
                )?;
                break;
            }
        }
    }

    Ok(())
}

pub fn simple_construction_project_selection(
    logger: &Logger,
    ai_interface: AIInterface,
    frame: i32,
    team_units: &[Unit],
    friendly_units: &[Unit],
    enemy_units: &[Unit],
    unit: Unit,
    proj_type: ProjectType,
) -> Result<(), Box<dyn Error>> {
    match proj_type {
        ProjectType::Commander => info!(logger, "Commander starting project"),
        ProjectType::Constructor => info!(logger, "Constructor starting project"),
        ProjectType::Factory => info!(logger, "Factory starting project"),
    }

    let ai_type = ai_interface.load_memory::<AIType>("AIType").unwrap();

    let mut energy_current = 0.0;
    let mut energy_storage = 0.0;
    let mut metal_current = 0.0;
    let mut metal_storage = 0.0;

    let mut success = false;

    let resources = ai_interface.resource_interface().get_resources()?;
    let mut mex_spots = Vec::new();
    for resource in ai_interface.resource_interface().get_resources()? {
        if resource.name()? == "Energy" {
            energy_current = resource.current()?;
            energy_storage = resource.storage()?;
        } else if resource.name()? == "Metal" {
            metal_current = resource.current()?;
            metal_storage = resource.storage()?;

            let position = unit.position()?;
            mex_spots = ai_interface
                .map()
                .resource_spots_map(resource)?
                .into_iter()
                .map(|(location, _)| location)
                .collect::<Vec<_>>();
        }
    }

    let extractor_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleExtractorDefs")
        .unwrap();
    let turret_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleTurretDefs")
        .unwrap();
    let generator_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleGeneratorDefs")
        .unwrap();
    let constructor_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleConstructorDefs")
        .unwrap();
    let undefined_unit_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleUndefinedUnitDefs")
        .unwrap();
    let undefined_building_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleUndefinedBuildingDefs")
        .unwrap();
    let factory_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleFactoryDefs")
        .unwrap();

    let t1_mexes = ai_interface
        .load_memory::<usize>("SimpleT1Mexes")
        .unwrap_or_default();
    let factory_count = ai_interface
        .load_memory::<usize>("SimpleFactoryCount")
        .unwrap_or(0);

    for _ in 0..10 {
        if proj_type == ProjectType::Constructor || proj_type == ProjectType::Commander {
            let mut factory_delay = ai_interface
                .load_memory::<isize>("SimpleFactoryDelay")
                .unwrap_or_default()
                - 1;
            ai_interface.store_memory("SimpleFactoryDelay", &factory_delay);

            let r = random_number(0.0, 20.0) as i32;

            let mex_spots = sorted_distance_to(&mex_spots, unit.position()?)?
                .into_iter()
                .filter_map(|(location, _)| {
                    if nearby_units(location, 32.0, team_units, friendly_units, enemy_units) {
                        None
                    } else {
                        Some(location)
                    }
                })
                .collect::<Vec<_>>();
            let mex_spot_opt = mex_spots.first().cloned();

            if t1_mexes < 3 && proj_type == ProjectType::Commander {
                if let Some(mex_spot) = mex_spot_opt {
                    let project = random_choice(&extractor_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random mex");
                        unit.build(
                            &[UnitCommandOptions::ShiftKey],
                            None,
                            Some(frame + 10000),
                            project,
                            GroundPosition(mex_spot.0, mex_spot.1),
                            Facing::None,
                        )?;
                        success = true;
                    }
                }
            } else if energy_current < energy_storage * 0.75 || r == 0 {
                if let Some(mex_spot) = mex_spot_opt {
                    let project = random_choice(&generator_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random generator");
                        simple_build_order(
                            logger,
                            ai_interface,
                            frame,
                            team_units,
                            friendly_units,
                            enemy_units,
                            unit,
                            project,
                        )?;
                        success = true;
                    }
                }
            } else if proj_type != ProjectType::Commander
                && (metal_current < metal_storage * 0.3 || r == 1)
            {
                // TODO: if no free mex and (energy_current > energy_storage * 0.85 || r == 1)
                // then
                //     project = random converter
                //     simple_build_order(logger, ai_interface, unit, project)?;
                //     success = true;
                // else if there is a free mex spot proj_type != ProjectType::Commander
                // if proj_type != ProjectType::Commander {
                if let Some(mex_spot) = mex_spot_opt {
                    let project = random_choice(&extractor_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building defended mex");
                        unit.build(
                            &[UnitCommandOptions::ShiftKey],
                            None,
                            Some(frame + 10000),
                            project,
                            GroundPosition(mex_spot.0, mex_spot.1),
                            Facing::None,
                        )?;
                        success = true;
                        let x_offsets = [0.0, 100.0, -100.0];
                        let z_offsets = [0.0, 100.0, -100.0];
                        for x_offset in x_offsets {
                            for z_offset in z_offsets {
                                let project_turret = random_choice(&turret_defs);
                                if unit
                                    .unit_def()?
                                    .building()
                                    .build_options()?
                                    .contains(&project_turret)
                                {
                                    unit.build(
                                        &[UnitCommandOptions::ShiftKey],
                                        None,
                                        Some(frame + 10000),
                                        project_turret,
                                        GroundPosition(
                                            mex_spot.0 + x_offset,
                                            mex_spot.1 + z_offset,
                                        ),
                                        Facing::None,
                                    )?;
                                }
                            }
                        }
                    }
                }
                // }
            } else if (2..=5).contains(&r) {
                let project = random_choice(&turret_defs);
                if unit
                    .unit_def()?
                    .building()
                    .build_options()?
                    .contains(&project)
                {
                    info!(logger, "Building turret");
                    simple_build_order(
                        logger,
                        ai_interface,
                        frame,
                        team_units,
                        friendly_units,
                        enemy_units,
                        unit,
                        project,
                    )?;
                    success = true;
                }
            } else if factory_count < 1
                || (metal_current > metal_storage * 0.75
                    && energy_current > energy_storage * 0.75
                    && factory_delay <= 0)
            {
                let project = random_choice(&factory_defs);
                if unit
                    .unit_def()?
                    .building()
                    .build_options()?
                    .contains(&project)
                {
                    info!(logger, "Building factory");

                    simple_build_order(
                        logger,
                        ai_interface,
                        frame,
                        team_units,
                        friendly_units,
                        enemy_units,
                        unit,
                        project,
                    )?;

                    factory_delay = 30;
                    ai_interface.store_memory("SimpleFactoryDelay", &factory_delay);

                    success = true;
                }
            } else if proj_type != ProjectType::Commander && (11..=12).contains(&r) {
                let units = ai_interface.unit_interface().team_units()?;
                for _ in 0..10 {
                    let target_unit = random_choice(&units);
                    if !target_unit.unit_def()?.movement().able_to_move()? {
                        info!(logger, "Assisting building/Reclaiming/Repairing");
                        let target_position = target_unit.position()?;
                        unit.fight(
                            &[
                                UnitCommandOptions::ShiftKey,
                                UnitCommandOptions::AltKey,
                                UnitCommandOptions::ControlKey,
                            ],
                            None,
                            Some(frame + 1000),
                            GroundPosition(
                                target_position[0] + random_number(-100.0, 100.0),
                                target_position[2] + random_number(-100.0, 100.0),
                            ),
                        )?;
                        success = true;
                        break;
                    }
                }
            } else {
                if random_choice(&[false, true]) {
                    let project = random_choice(&undefined_building_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random undefined building");
                        simple_build_order(
                            logger,
                            ai_interface,
                            frame,
                            team_units,
                            friendly_units,
                            enemy_units,
                            unit,
                            project,
                        )?;
                        success = true;
                    }
                } else {
                    let project = random_choice(&turret_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random turret");
                        simple_build_order(
                            logger,
                            ai_interface,
                            frame,
                            team_units,
                            friendly_units,
                            enemy_units,
                            unit,
                            project,
                        )?;
                        success = true;
                    }
                }
            }
        } else {
            if unit.current_commands()?.len() < 5 {
                let r = random_number(0.0, 5.0) as usize;
                if r == 0
                    || metal_current > metal_storage * 0.9
                    || ai_type == AIType::SimpleConstructorAI
                {
                    let project = random_choice(&constructor_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random constructor");
                        unit.build(
                            &[],
                            None,
                            Some(frame + 10000),
                            project,
                            unit.position()?,
                            Facing::None,
                        )?;
                        success = true;
                    }
                } else {
                    let project = random_choice(&undefined_unit_defs);
                    if unit
                        .unit_def()?
                        .building()
                        .build_options()?
                        .contains(&project)
                    {
                        info!(logger, "Building random army unit");
                        unit.build(
                            &[],
                            None,
                            Some(frame + 10000),
                            project,
                            unit.position()?,
                            Facing::None,
                        )?;
                        success = true;
                    }
                }
            } else {
                success = true;
            }
        }
        if success {
            break;
        }
    }

    Ok(())
}
